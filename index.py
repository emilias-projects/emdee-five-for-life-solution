from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import hashlib

browser = webdriver.Chrome()

browser.get("https://md5.emilia.tf")
assert 'Emdee Five For Life' in browser.title

elem = browser.find_element(By.ID, 'target')
print(elem.text)
inputelem = browser.find_element(By.ID, 'md5')
inputelem.send_keys(hashlib.md5(elem.text.encode("utf")).hexdigest())
button = browser.find_element(By.CSS_SELECTOR, 'button')
button.click()
